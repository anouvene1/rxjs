import { Component, OnInit } from '@angular/core';

import { MyCustomObserver } from './my-custom-observer'
import { Observable, from, interval, throwError, of, fromEvent, combineLatest, forkJoin, merge } from 'rxjs';
import {concatMap, switchMap,  catchError,  map,  mapTo,  take,  tap,  filter,  concat} from 'rxjs/internal/operators';

@Component({
  selector: 'rx-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent implements OnInit {
  
  title = 'rxjs-basics';

  count: number = 0;

  numbers: number[] = [1,2,3,4,5];

  ngOnInit(): void {
    // const numbers$ = from(this.numbers);

    // numbers$.subscribe(new MyCustomObserver()); // Observer personnalisé

    // Ou bien utiliser un Observer natif
    // numbers$.subscribe({
    //   next: (val:number) => { console.log(`Valeur: ${val}`); },
    //   error: ()=> {},
    //   complete: () => { console.log(`C'est fini !!`); }
    // });

    // Une autre syntaxe simplifiée:
    // numbers$.subscribe(
    //   (val:number) => { console.log(`Valeur: ${val}`); },
    //   (error)=> { console.log(`Erreur: ${error}`); },
    //   () => { console.log(`C'est fini !!`); }
    // );

    /*=========================================================================
    // CREER UN FLUX
    ===========================================================================*/
    // const fruits = ["Pomme", "Poire", "Cerise", "Ananas", "Kiwi"];
    // const fruits$ = Observable.create((observer: any) => {
    //   for(let f of  fruits) {
    //     observer.next(f);
    //   }
    //   // Appeler complete quand il n'y plus rien à emettre
    //   observer.complete(() => { console.log("C'est fini !!"); });
    // });

    // Souscrire à l'observable ainsi crée
    // fruits$.subscribe( (val: string) => {
    //   console.log("Fruit: " + val);
    // });

     /*=========================================================================
    // INTERVAL : opérateur de cration à interval régulier
    // TAKE : produire un certain nombre de données puis s'arrête à complete()
    ===========================================================================*/
    // let timer$ = interval(500);
    // timer$.pipe(take(5)).subscribe(
    //   val => console.log(val),// 0 1 2 3 4
    //   err => console.log(err),
    //   () => console.log("Fini !!")
    // );

    /*===========================================================================
    // Operateur de creation "from" avec une promesse
    =============================================================================*/
    // const unePromesse:Promise<string> = new Promise( resolve => resolve("Résolu !!"));
    // from(unePromesse).subscribe( (msg: string) => console.log(msg));

    /*===========================================================================
    // FromEvent: flux de données issus des évènements
    // Do: opérateur de débuggage des observables
    =============================================================================*/
    // let clickMe$ = fromEvent<MouseEvent>(document.querySelector("#clickMe"), "click");
    // let coord$ = clickMe$.pipe(
    //   tap( data => console.log("Avant map", data) ),
    //   map( (evt: MouseEvent) => ({x: evt.clientX, y: evt.clientY}) ), // {x: 953, y: 247}
    //   tap( data => console.log("Après map", data) ), // {x: 953, y: 247}
    // );

    // coord$.subscribe(
    //   val => console.log("Event click coordonnées: ", val),// {x: 953, y: 247}
    //   err => console.log(err),
    //   () => console.log("Voilà, c'est fini !!")
    // );

    /*===========================================================================
    // Operateurs de transformation map et mapTo
    =============================================================================*/
    // const fruitsBis$ = of("Pomme", "Poire", "Cerise", "Ananas", "Kiwi");
    // fruitsBis$.pipe(map((f) => { f += " - origine France"; return f; })).subscribe(fruit => console.log(fruit));

    // // mapTo permet de traiter plus librement les valeurs que le map
    // fruitsBis$.pipe(mapTo("Fruits made in France")).subscribe(fruit => console.log(fruit));

    /*===========================================================================
    // Operateur de transformation "switchMap": créer un nouveau flux 
    // à partir d'un autre flux
    =============================================================================*/
    // const noms$ = from(["Joséphine", "Ben Lesh", "", "Sophia"]);
    // noms$.pipe(
    //   switchMap( (nom: any) => {
    //     if(nom === "") {
    //       //catchError((e)=> {
    //           return throwError("Chaine vide!!");
    //       //})
    //     }
    //     return of(nom);
    //   })
    // ).subscribe( 
    //     n => { console.log("Nom: ", n); },
    //     err => { console.log("Erreur: ", err); },
    //     () => { console.log("C'est fini !!"); }
    // );

    /*===========================================================================
    // filter: Operateur de filtre
    // Ici filtrer les donnees non vides
    =============================================================================*/
    // const brands$ = from(["CKlein", "Le temps de cerises", "", "Nike", "", "Adidas"]);
    // brands$.pipe(
    //   filter( brand => brand !== "")
    // ).subscribe( data => console.log(data)); // CKlein Le temps de cerises Nike


    /*===========================================================================
    // concat: Operateur de concaténation
    // Ici concaténer un flux de chiffres et de chaines
    =============================================================================*/
    // const num$ = from([1, 2, 3]);
    // const alpha$ = of('a', 'b', 'c');

    // const alphaNum$ = num$.pipe(
    //   concat(alpha$)
    // );

    // alphaNum$.subscribe( (val:string|number) => console.log(val) ); // 1 2 3 a b c


    /*===========================================================================
    // combinelatest: Operateur qui prend en parametres plusieurs Observables et 
    // va émettre les dernières valeurs émises de chacun des Observables
    // Il s'arrête d'émettre lorsque au moins un des Observables a fini d'émettre
    =============================================================================*/
    // const streamA$ = this.createReadStream("A", 2, 1000);
    // const streamB$ = this.createReadStream("B", 1, 500);
    // const obs$ = combineLatest(streamA$, streamB$);
    // obs$.subscribe( val => console.log("Combine latest: ", val));

    /*===========================================================================
    // concatMap: prend en parametre une fonction de traitement et retourne un
    // nouveau Observable
    =============================================================================*/
    // const click$ = fromEvent(document.querySelector("#clickMe"), "click");
    // const result$ = click$.pipe(
    //   concatMap( evt => {
    //     return interval(1000).pipe( take(4));
    //   })
    // );

    // result$.subscribe( val => console.log(val));

    /*===========================================================================
    // forkJoin: émet la derniere valeur de chaque observable passé en parametre
    // Il peut également prendre en dernier paramètre une fonction de projection 
    // pour effectuer les opérations avec les valeurs retournées
    =============================================================================*/

    // const stream1$ = this.createReadStream("A", 2, 1000);
    // const stream2$ = this.createReadStream("B", 4, 500);
    // const stream3$ = this.createReadStream("C", 6, 250);

    // const streams$ = forkJoin(stream1$, stream2$, stream3$);
    // streams$.subscribe( val => console.log(val));

    /*===========================================================================
    // Exemple de compteur avec 2 boutons "+" ou "-" 
    // et un champ affichant le nombre incrémenté ou décrémenté
    // 
    =============================================================================*/

    // let click$ = fromEvent(document, "click");
    // let clickPlus$ = click$.pipe(
    //   filter( evt => (<HTMLInputElement>evt.target).id === "plus")
    // );
    // let clickMinus$ = click$.pipe(
    //   filter( evt => (<HTMLInputElement>evt.target).id === "minus")
    // );

    // let totPlus$ = clickPlus$.pipe(
    //   mapTo(1)
    // );

    // let totMinus$ = clickMinus$.pipe(
    //   mapTo(-1)
    // );


    // Or 
    let clickPlus$ = fromEvent(document.querySelector("#plus"), "click").pipe(mapTo(1)); 
    let clickMinus$ = fromEvent(document.querySelector("#minus"), "click").pipe(mapTo(-1)); 

    let plusMinus$ = merge(clickPlus$, clickMinus$);
    //let plusMinus$ = merge(totPlus$, totMinus$);
    plusMinus$.subscribe( 
      val => this.countPlusMinus(val),
      err => console.log(err),
      () => console.log("Fin")
    );



  }

  /*===========================================================================
  // Génerateur de flux: 
  // interval: role de générateur  qui émet les valeurs 0, 1, 2 ... 
  // toutes les x intervalle (en milliseconde)
  // take: arrête l'emission de valeurs par un Observable au bout de y itérations
  // tap: ici pour inspecter les valeurs émises par un Observable
  =============================================================================*/
  createReadStream(streamName: string, iterations: number, intervalle: number): Observable<any> {
    return interval(intervalle).pipe(
      take(iterations),
      tap( val => console.log(`stream ${streamName}: ${val}`))
    );
  }

  /**
   * Count increment or decrement clicks
   * @param val number Total of counter
   */
  countPlusMinus(val: number) {
    this.count += val; 
  }


}
