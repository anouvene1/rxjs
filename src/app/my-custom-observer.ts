export class MyCustomObserver {
    next(val: any) {
        console.log(`Value: ${val}`);
    }

    error(error: any) {
        console.log(`Error: ${error}`);
    }

    complete() {
        console.log('Complete');
    }
}